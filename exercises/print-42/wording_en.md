Write a single line of code displaying the number 42.

## What you'll learn

In this exercise you'll learn to *use* a function by trying the
[`print`](https://docs.python.org/3/library/functions.html#print) builtin
function.

A function is a named thing that does something specific:

- the `print` function displays what's given to it,
- the `len` function measures the thing we give to it,
- …

The syntax to make a function work is called a "function call", to make `print`
display the string `"Hello world"` the syntax is:

```python
print("Hello world")
```

## Advice

You'll need a [number](https://docs.python.org/3/tutorial/introduction.html#numbers)
and the [`print()`](https://docs.python.org/3/library/functions.html#print) builtin function.

I'm not asking to print the string "42", (composed of two chars), but the number 42.

Don't hesitate to hit the "Run" button, it only runs your code and display the
result (on the right), exactly like if you were executing a Python file with a
Python interpreter. Do as many test as you like, until you feel confident you
understand how `print` works.

Once you feel confident, hit the blue "Submit" button: this one will send your
code to my bot, which will check if everything is right, and tell you if
it's OK.
