Écrivez une seule ligne de code affichant le nombre `42`.

## Ce qu'on va apprendre

Dans cet exercice, on apprend à *utiliser* une fonction, en essayant la fonction
native [`print`](https://docs.python.org/fr/3/library/functions.html#print).

Une fonction est une chose, nommée, qui fait quelque chose de bien spécifique :

- la fonction `print` affiche ce qu'on lui donne,
- la fonction `len` mesure ce qu'on lui donne,
- …

La syntaxe pour faire fonctionner une fonction s'appelle « un appel de
fonction », par exemple pour demander à la fonction `print` d'afficher la chaîne
de caractères `"Hello world"`, la syntaxe est :

```python
print("Hello world")
```


## Conseils

Vous aurez besoin d'un
[nombre](https://docs.python.org/fr/3/tutorial/introduction.html#numbers)
et de la fonction native
[`print()`](https://docs.python.org/fr/3/library/functions.html#print).

Attention, on ne vous demande pas d'afficher la [chaîne de
caractères](https://docs.python.org/fr/3/tutorial/introduction.html#strings)
`"42"`, composée de deux caractères, mais bien le nombre 42, vous
n'aurez donc pas besoin de guillemets dans cet exercice.

N'hésitez pas à cliquer sur le bouton gris "Exécuter", il ne fait
qu'exécuter votre code et vous afficher le résultat (à droite), comme
si vous éditiez un fichier Python sur votre machine, ça vous permet
d'expérimenter.

Dès que le résultat vous convient, vous pouvez utiliser le bouton bleu
« Envoi pour validation » pour proposer votre code à la moulinette de
correction.
