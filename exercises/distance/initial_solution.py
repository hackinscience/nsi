def dist(points):
    ...


if __name__ == "__main__":
    assert dist([1, 2, 3]) == 2
    assert dist([1, 2, 3, 2.5]) == 2
    assert dist([1, 2, 3, 2.5, 3.5]) == 2.5
    assert dist([1, 2, 3, 2.5, 3.5, 120]) == 119
    assert dist([1, 2, 3, 2.5, 3.5, 120, -1000]) == 1120
