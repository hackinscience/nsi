from itertools import permutations

import correction_helper as checker
from math import isclose

checker.exclude_file_from_traceback(__file__)

with checker.student_code():
    from solution import dist


def my_dist(data):
    return max(data) - min(data)


def check(test):
    with checker.student_code(print_allowed=None):
        their = dist(test)

    if their is None:
        checker.fail(
            f"Votre function, appellée `dist({test!r})` renvoie `None`.",
            "Elle doit renvoyer la distance entre les deux valeurs "
            "les plus éloignées.",
        )
    expected = my_dist(test)
    if not isclose(their, expected):
        tail = ""
        if len(test) == 1:
            tail = """(Lorsqu'il n'y a qu'un point, la distance entre lui et lui même
est de zéro)"""
        checker.fail(
            f"La plus grande distance dans `{test!r}` n'est pas "
            f"`{type(their).__name__}` `{their}` "
            f"mais `{type(expected).__name__}` `{expected}`",
            tail,
        )


def main():
    seed = [1, 2, 12.5, 22, 0.01, -9]
    for i in range(2, 6):
        for test in permutations(seed, i):
            check(list(test))
    check([-1, -2, -3])


if __name__ == "__main__":
    main()
