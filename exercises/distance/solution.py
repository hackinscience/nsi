def dist(iterable):
    return max(iterable) - min(iterable)
